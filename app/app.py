from flask import Flask, request
from flask_restful import Resource, Api, reqparse
import werkzeug
import base64
import cv2
import os
import numpy as np
from PIL import Image
from models.sa_gan import STRnet2

import torch
from torchvision.transforms import Compose, ToTensor, Resize
import torchvision.transforms as T

app = Flask(__name__)

pretrained_path = 'ckp/model.pth'
loadSize=(512,512)

netG = STRnet2(3)
cuda = torch.cuda.is_available()
if cuda:
    netG.load_state_dict(torch.load(pretrained_path))
    netG = netG.cuda()
else:
    netG.load_state_dict(torch.load(pretrained_path, map_location=torch.device('cpu')))


def ImageTransform(loadSize):
    return Compose([
        Resize(size=loadSize, interpolation=Image.BICUBIC),
        ToTensor(),
    ])
ImgTrans = ImageTransform(loadSize)

def create_image():
    parse = reqparse.RequestParser()
    parse.add_argument('image', type=werkzeug.datastructures.FileStorage, location='files')
    args = parse.parse_args()
    print(args)
    image_file = args['image']

    image = Image.open(image_file)

    H,W = image.size
    img = ImgTrans(image.convert('RGB'))
    img = img.unsqueeze(0)
    if cuda:
        img = img.cuda()
    _,_,_, g_images,_ = netG(img)
    transform = T.Resize((W,H))
    g_image = g_images.data.cpu()
    g_image = transform(g_image)
    g_image = g_image[0].numpy().transpose(1, 2, 0)
    g_image = cv2.cvtColor(g_image, cv2.COLOR_RGB2BGR)
    norm_image = cv2.normalize(g_image, None, alpha = 0, beta = 255, norm_type = cv2.NORM_MINMAX, dtype = cv2.CV_32F)

    g_image = norm_image.astype(np.uint8)
    _, buffer = cv2.imencode('.jpg', g_image)
    b64_string = base64.b64encode(buffer)
    return b64_string

@app.route('/text_removal', methods=['GET','POST'])
def programming_languages_route():
    if request.method == "GET":
        return "hello"
    elif request.method == "POST":
        return create_image()
    
if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
    